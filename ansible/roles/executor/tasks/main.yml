---
- name: Installing dependencies
  tags: install
  ansible.builtin.include_role:
    name: pacman_install
  vars:
    pacman_install_pkgs:
      - gcc  # easysnmp needs gcc to compile
      - net-snmp
      - python
      - python-pip

- name: Create the executor group
  become: true
  ansible.builtin.group:
    name: executor

- name: Create the executor user
  become: true
  ansible.builtin.user:
    name: executor
    group: executor
    shell: /bin/nologin

- name: Creates /app/valve-infra/executor/
  ansible.builtin.file:
    path: /app/valve-infra/executor/
    state: directory
    mode: '0755'

- name: Copy package
  ansible.posix.synchronize:
    src: "{{ executor_package_location }}"
    dest: /app/valve-infra/executor/
    rsync_opts:
      - "--exclude=.tox"
      - "--exclude=.venv"
      - "--exclude=htmlcov"
      - "--exclude=__pycache__"
      - "--exclude=*.pyc"
  register: executor_package

- name: Install executor # noqa no-handler
  ansible.builtin.pip:
    name: "file:///app/valve-infra/executor/server"
  notify: 'Restart executor'
  # Always install when building the container, since the previous task won't
  # result in a "changed" status.
  # This is necessary because pip installing a local dist is never idempotent,
  # see: https://github.com/pypa/pip/pull/9147
  when: pid_1_name != "systemd" or executor_package.changed

- name: Install executor systemd unit file
  ansible.builtin.template:
    src: executor_systemd_unit.j2
    dest: /etc/systemd/system/executor.service
    owner: root
    group: root
    mode: '0644'
  notify: 'Restart executor'

- name: Enable executor service
  become: true
  ansible.builtin.systemd:
    name: 'executor'
    enabled: true

- name: configuring env
  become: true
  ansible.builtin.blockinfile:
    create: true
    mode: '0644'
    path: "{{ base_config_env_file }}"
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ role_name }}"
    block: |
      PRIVATE_INTERFACE=private
      CONSOLE_PATTERN_DEFAULT_MACHINE_UNFIT_FOR_SERVICE_REGEX="FATAL ERROR: The local machine doesn't match its expected state from MaRS\n$"
      EXECUTOR_ARTIFACT_CACHE_ROOT={{ tmp_mount }}/executor/artifact_cache
      EXECUTOR_HOST=0.0.0.0
      EXECUTOR_PORT=80
      SERGENT_HARTMAN_BOOT_COUNT={{ sergent_hartman_boot_count }}
      SERGENT_HARTMAN_QUALIFYING_BOOT_COUNT={{ sergent_hartman_qualifying_boot_count }}
      SERGENT_HARTMAN_REGISTRATION_RETRIAL_DELAY={{ sergent_hartman_registration_retrial_delay }}
      GITLAB_CONF_FILE=/etc/gitlab-runner/config.toml
      BOOTS_DEFAULT_KERNEL=http://{{ hostname }}:{{ minio_port }}/boot/default_kernel
      BOOTS_DEFAULT_INITRD=http://{{ hostname }}:{{ minio_port }}/boot/default_boot2container.cpio.xz
      BOOTS_DEFAULT_CMDLINE=b2c.container="-ti --tls-verify=false docker://{{ hostname }}:{{ fdo_proxy_registry_port }}/mupuf/valve-infra/machine_registration:latest register" b2c.ntp_peer="{{ hostname }}" b2c.cache_device=none loglevel=6 b2c.poweroff_delay=15
      MARS_DB_FILE=/mnt/permanent/mars_db.yaml
      # Variables with the "EXECUTOR_JOB__" prefix are shared by executor with
      # the job after removing the prefix and converting them to lower case

  notify: 'Restart executor'
