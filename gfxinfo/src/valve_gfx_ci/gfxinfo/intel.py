from dataclasses import dataclass

from . import PCIDevice


@dataclass
class IntelGPU:
    pci_device: PCIDevice
    gen_version: int
    family: str

    @property
    def base_name(self):
        return 'intel' + self.gen_version

    @property
    def pciid(self):
        return str(self.pci_device)

    @property
    def tags(self):
        return {
            f"intelgpu:pciid:{self.pciid}",
            f"intelgpu:family:{self.family}",
            f"intelgpu:gen:{self.gen_version}",
        }

    @property
    def structured_tags(self):
        return {
            "type": "intelgpu",
            "family": self.family,
            "gen": self.gen_version
        }

    def __str__(self):
        return f"<IntelGPU: PCIID {self.pciid} - gen{self.gen_version} - {self.family}>"


class IntelGpuDeviceDB:
    def cache_db(self):
        # NOTHING TO DO
        pass

    def update(self):
        # NOTHING TO DO
        pass

    def check_db(self):
        return True

    def from_pciid(self, pciid):
        if pciid.vendor_id != 0x8086:
            return None

        SUPPORTED_GPUS = {
            0x3e9b: {
                'gen_version': '9',
                'family': 'COFFEELAKE'
            },
        }
        if md := SUPPORTED_GPUS.get(pciid.product_id):
            return IntelGPU(pci_device=pciid, **md)
