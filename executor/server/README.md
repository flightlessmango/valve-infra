# valve-infra executor

The Valve Infra executor is the service that coordinates different services to
enable time-sharing test machines, AKA devices under test (DUTs).

This service can be interacted with using
[executorctl](https://gitlab.freedesktop.org/mupuf/valve-infra/-/blob/master/executor/client/),
our client, and/or our
[REST API](https://mupuf.pages.freedesktop.org/valve-infra/docs/executor.html#id1).

Take a look to our full documentation at
[https://mupuf.pages.freedesktop.org/valve-infra/](https://mupuf.pages.freedesktop.org/valve-infra/)
